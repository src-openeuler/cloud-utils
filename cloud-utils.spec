Name:            cloud-utils
Version:         0.33
Release:         2
Summary:         Cloud image management utilities
License:         GPLv3
URL:             https://github.com/canonical/cloud-utils
Source0:         https://github.com/canonical/cloud-utils/archive/refs/tags/%{version}.tar.gz
Requires:        cloud-utils-growpart gawk e2fsprogs file python3
Requires:        qemu-img util-linux

%description
This package provides a useful set of utilities for managing cloud images.

%package growpart
Summary:         Script for growing a partition
Requires:        gawk util-linux

%description growpart
This package provides the growpart script for growing a partition.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%install
%make_install

%files
%defattr(-,root,root)
%license LICENSE
%doc ChangeLog
%{_bindir}/cloud-localds
%{_bindir}/ec2metadata
%{_bindir}/mount-image-callback
%{_bindir}/resize-part-image
%{_bindir}/ubuntu-cloudimg-query
%{_bindir}/vcs-run
%{_bindir}/write-mime-multipart

%files growpart
%defattr(-,root,root)
%license LICENSE
%doc ChangeLog
%{_bindir}/growpart

%files help
%defattr(-,root,root)
%{_mandir}/man1/*

%changelog
* Tue Oct 25 2022 yanglongkang <yanglongkang@h-partners.com> - 0.33-2
- rebuild for next release

* Mon Sep 19 2022 panxiaohe <panxh.life@foxmail.com> - 0.33-1
- update version to 0.33

* Wed Dec 29 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 0.32-1
- update version to 0.32

* Mon Feb 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.31-1
- Package init
